﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FindDecimal : MonoBehaviour
{
    int dividend;

    [SerializeField]
    InputField dividendInputField;

    int divisor;

    [SerializeField]
    InputField divisorInputField;

    int positionOfDecimalToFind;

    [SerializeField]
    InputField positionOfDecimalToFindInputField;

    [SerializeField]
    Text answerText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void EnterInput() 
    {
        if (dividendInputField.text == "" || divisorInputField.text == "" || positionOfDecimalToFindInputField.text == "")
        {
            answerText.text = "Please Input Every field";
        }
        else 
        {
            int.TryParse(dividendInputField.text, out dividend);
            int.TryParse(divisorInputField.text, out divisor);
            int.TryParse(positionOfDecimalToFindInputField.text, out positionOfDecimalToFind);
            FindDecimalPlace();
        }
    }

    void FindDecimalPlace() 
    {
        if (divisor == 0)
        {
            answerText.text = "Infinity";
            return;
        }
        if (divisor == 0)
        {
            answerText.text = "0";
            return;
        }
        if (positionOfDecimalToFind <= 0)
        {
            answerText.text = "positionOfDecimalToFind must be over 0";
            return;
        }

        // Integral division 
        int decimalPoint = dividend / divisor;

        // Now one by print digits after dot 
        // using school division method. 
        for (int i = 0; i <= positionOfDecimalToFind; i++)
        {
            dividend = dividend - (divisor * decimalPoint);
            if (dividend == 0) 
            {
                answerText.text = "The answer is " + 0;
                break;
            }
            if (i == positionOfDecimalToFind)
            {
                answerText.text = "The answer is " + decimalPoint;
            }
            dividend = dividend * 10;
            decimalPoint = dividend / divisor;


        }
    }
}
