﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void GoToQ1() 
    {
        SceneManager.LoadScene("Q1");
    }

    public void GoToQ2()
    {
        SceneManager.LoadScene("Q2");
    }

    public void Exit() 
    {
        Application.Quit();
    }
}
