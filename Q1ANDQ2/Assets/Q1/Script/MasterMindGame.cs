﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class MasterMindGame : MonoBehaviour
{
    int[] numbersToBeGuess = new int[4];

    [SerializeField]
    InputField inputField;

    int[] numbers = new int[4];

    #region UIText

    [SerializeField]
    Text totalCorrectNumberText;

    [SerializeField]
    Text totalCorrectPositionText;

    [SerializeField]
    Text totalTriesText;

    #endregion

    #region IntStorer

    int totalCorrectNumber;

    int totalCorrectPosition;

    int totalTries;

    #endregion

    [SerializeField]
    GameObject RightUI;

    [SerializeField]
    GameObject WrongUI;

    void Start()
    {
        GenerateNumber();
        inputField.characterLimit = numbersToBeGuess.Length;
    }

    void GenerateNumber() 
    {
        for(int i = 0; i < numbersToBeGuess.Length; i++) 
        {
            numbersToBeGuess[i] = UnityEngine.Random.Range(0, 9);
            Debug.Log(numbersToBeGuess[i]);
        }
    }

    public void NewGame() 
    {
        GenerateNumber();
        inputField.characterLimit = numbersToBeGuess.Length;
        totalTries = 0;
        Reset();
    }

    public void TryAgain() 
    {
        Reset();
    }

    private void Reset()
    {
        totalCorrectNumber = 0;
        totalCorrectPosition = 0;
        RightUI.SetActive(false);
        WrongUI.SetActive(false);
    }

    public void EnterTheAnswer()
    {
        numbers = Array.ConvertAll(inputField.text.ToCharArray(), c => (int)Char.GetNumericValue(c));
        Debug.Log("inputField.text.ToString().Length " + inputField.text.ToString().Length);
        Debug.Log("numbersToBeGuess.Length " + numbersToBeGuess.Length);
        if (inputField.text.Length == numbersToBeGuess.Length) 
        {
            CheckCorrectNumbers();
            CheckCorrectPosition();
            totalTries++;
            totalTriesText.text = totalTries.ToString();

            if (totalCorrectNumber == 4 && totalCorrectPosition == 4)
            {
                RightUI.SetActive(true);
            }
            else
            {
                WrongUI.SetActive(true);
            }
        }
        inputField.text = "";
    }

    void CheckCorrectNumbers() 
    {
        for (int i = 0; i < numbersToBeGuess.Length; i++)
        {
            for (int j = 0; j < inputField.text.ToString().Length; j++)
            {
                if (numbersToBeGuess[i] == numbers[j])
                {
                    totalCorrectNumber++;
                    break;
                }
            }
        }
        totalCorrectNumberText.text = totalCorrectNumber.ToString();
    }

    void CheckCorrectPosition()
    {
        for (int i = 0; i < numbersToBeGuess.Length; i++)
        {
           if (numbersToBeGuess[i] == numbers[i])
           {
                totalCorrectPosition++;
                continue;
           }
        }
        totalCorrectPositionText.text = totalCorrectPosition.ToString();
    }
}
